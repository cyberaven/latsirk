﻿public interface ISetSide
{
    Side Side { get; set; }
    void SetSide(Side side);
}
