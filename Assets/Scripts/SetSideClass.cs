﻿using UnityEngine;
using System.Collections;

public class SetSideClass : MonoBehaviour, ISetSide
{    
    public Side Side { get; set; }

    public void SetSide(Side setSide)
    {
        Side = setSide;
    }
}
