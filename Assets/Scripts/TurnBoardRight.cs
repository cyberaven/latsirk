﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TurnBoardRight : MonoBehaviour
{
    public delegate void StartTurnBoardRightHandler();
    public static event StartTurnBoardRightHandler StartTurnBoardRightEvent;

    public delegate void StopTurnBoardRightHandler();
    public static event StopTurnBoardRightHandler StopTurnBoardRightEvent;


    private void OnMouseDown()
    {
        if (StartTurnBoardRightEvent != null)
        {
            StartTurnBoardRightEvent();
        }
    }
    private void OnMouseUp()
    {
        if (StopTurnBoardRightEvent != null)
        {
            StopTurnBoardRightEvent();
        }
    }
}
