﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Board : MonoBehaviour
{
    //public BoardCube _boardCube;
    //public List<BoardCube> boardCubesList;

    public List<Material> boardCubeMaterialList;

    int boardSizeX = 10;
    int boardSizeZ = 10;
    float boardCenterOffset = 0.5f;

    bool rotateLeft = false;
    bool rotateRight = false;
    float rotateAngle;
    float rotateSpeed = 2f;

    void Start()
    {
        TurnBoardLeft.StartTurnBoardLeftEvent += StartRotateBoardLeft;
        TurnBoardLeft.StopTurnBoardLeftEvent += StopRotateBoardLeft;
        TurnBoardRight.StartTurnBoardRightEvent += StartRotateBoardRight;
        TurnBoardRight.StopTurnBoardRightEvent += StopRotateBoardRight;

        //boardCubesList = new List<BoardCube>();        

        CreateBoard();
    }

    private void Update()
    {
        RotateLeft();
        RotateRight();
    }

    void CreateBoard()
    {
        for (int x = -boardSizeX/2; x < boardSizeX/2; x++)
        {
            for (int z = -boardSizeZ/2; z < boardSizeZ/2; z++)
            {
                //BoardCube boardCube = Instantiate(_boardCube, transform);               
                //boardCubesList.Add(boardCube);                
                //boardCube.transform.localPosition = new Vector3(x+ boardCenterOffset, 0, z+ boardCenterOffset);
                //boardCube.GetComponent<Renderer>().material = RandomMaterialForBoardCube();
            }
        }
    }
    Material RandomMaterialForBoardCube()
    {
        Material result = boardCubeMaterialList[Random.Range(0, boardCubeMaterialList.Count)];
        return result;
    }

    #region RotateBoard
    void StartRotateBoardLeft()
    {
        rotateLeft = true;
        rotateRight = false;                      
    }
    void StopRotateBoardLeft()
    {
        rotateLeft = false;
    }
    void RotateLeft()
    {
        if (rotateLeft == true)
        {
            transform.Rotate(0, 1, 0);            
        }
    }
    void StartRotateBoardRight()
    {
        rotateRight = true;
        rotateLeft = false;             
    }
    void StopRotateBoardRight()
    {
        rotateRight = false;       
    }
    void RotateRight()
    {
        if (rotateRight == true)
        {
            transform.Rotate(0, -1, 0);
        }
    }
    #endregion
}
