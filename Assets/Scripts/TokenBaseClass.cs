﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class TokenBaseClass : MonoBehaviour
{
    protected ISetName setName;
    protected ISetPosition setPosition;
    protected ISetColorSide setColorSide;

    private ISetSide setSide;
    public Side side;

    
    public bool onBoard = false;
    public bool selected = false;

    public void Awake()
    {
        setName = gameObject.AddComponent<SetNameClass>();
        setPosition = gameObject.AddComponent<SetPositionClass>();
        setColorSide = gameObject.AddComponent<SetColorSideClass>();
        setSide = gameObject.AddComponent<SetSideClass>();
        
        //setName = new SetNameClass();        
        //setPosition = new SetPositionClass();
        //setColorSide = new SetColorSideClass();

        //setSide = new SetSideClass();
    }
    
    public void SetName()
    {
        setName.SetName();
    }
    public void SetPosition(float x, float y, float z)
    {
        setPosition.SetPosition(x, y, z);
    }
    public void SetColorSide(Side side)
    {
        setColorSide.SetColorSide(side);
    }

    public void SetSide(Side Side)
    {
        setSide.SetSide(Side);
        side = setSide.Side;
    }

    #region UnityMethod
    private void Start()
    {
        SetName();
    }
    #endregion
}
