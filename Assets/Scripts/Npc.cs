﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Npc : MonoBehaviour
{
    [SerializeField] TokenHand tokenHand;
    [SerializeField] List<Token> tokenList;
    [SerializeField] Token token;
    [SerializeField] Token selectedToken;
    [SerializeField] Token selectedToken2;
    [SerializeField] Token selectedTokenTower;
    [SerializeField] GameBoard gameBoard;
    [SerializeField] List<BoardCell> boardCellList;
    [SerializeField] BoardCell selectedBoardCell;
    public GameRuler gameRuler;

    private void Start()
    {
        name = "Npc";       
    }
    public void InitNpc(GameRuler gameRuler, TokenHand tokenHand, GameBoard gameBoard)
    {
        SetGameRuler(gameRuler);
        SetTokenHand(tokenHand);
        SetGameBoard(gameBoard);
    }
    void SetTokenHand(TokenHand setTokenHand)
    {
        tokenHand = setTokenHand;
        tokenList = tokenHand._tokenList;
    }
    void SetGameRuler(GameRuler setGameRuler)
    {
        gameRuler = setGameRuler;
    }
    void SetGameBoard(GameBoard setGameBoard)
    {
        gameBoard = setGameBoard;
        boardCellList = gameBoard._boardCellList;
    }

    private void Update()
    {
        AcceptMoveToken();
    }

    void AcceptMoveToken()
    {
        if(gameRuler.move == false)
        {            
            AnalyzeMove(); 
        }
    }    
    void AnalyzeMove()
    {
        int i = new int(); 
        i = Random.Range(0,8); //рандомный ход        
        
        if(!CheckTokenOnBoard())//проверяем есть ли нпц фишки на доске
        {
            i = 3;
        }

        if(i == 0)
        {
            Debug.Log("<color=red>NPC try selected similar token.</color>");
        }
        else if(i == 1)
        {
            Debug.Log("<color=red>NPC try select boardCell first.</color>");
        }
        else if (i == 2)
        {
            Debug.Log("<color=red>NPC try Put token in hand.</color>");
        }
        else if (i == 3)
        {
            Debug.Log("<color=blue>NPC try move token on board.</color>");

            selectedToken = SelectTokenOnBoard(false);
            selectedBoardCell = SelectBoardCell(); 
            MoveTokenOnBoard(selectedToken, selectedBoardCell);
            DeselectAll();
        }
        else if (i == 4)
        {
            Debug.Log("<color=blue>NPC try move token.</color>");
            selectedToken = SelectTokenOnBoard(true);
            selectedBoardCell = SelectBoardCell();
            MoveTokenOnBoard(selectedToken, selectedBoardCell);
            DeselectAll();
        }
        else if (i == 5)
        {
            Debug.Log("<color=blue>NPC try move token on token.</color>");
            int a = Random.Range(0, 2);

            if (!CheckMoreThanTwoTokenOnBoard())//проверяем есть ли две и более нпц фишки на доске
            {
                a = 1;
            }

            if (a == 1)
            {
                selectedToken = SelectTokenOnBoard(true);                
            }
            else if(a == 0)
            {
                selectedToken = SelectTokenOnBoard(false);                
            }           
            selectedToken2 = SelectTokenOnBoard(true);            
            MoveTokenOnToken(selectedToken, selectedToken2);
            DeselectAll();
        }
        else if(i == 6)
        {
            Debug.Log("<color=blue>NPC try move tokenTower.</color>");
            selectedTokenTower = SelectTokenOnBoard(true);
            selectedBoardCell = SelectBoardCell();
            MoveTokenTower(selectedTokenTower, selectedBoardCell);
            DeselectAll();
        }
        else if (i == 7)
        {
            Debug.Log("<color=blue>NPC try move tokenTower on token.</color>");            
            selectedTokenTower = SelectTokenOnBoard(true);
            selectedToken = SelectTokenOnBoard(true);
            MoveTokenTowerOnToken(selectedTokenTower, selectedToken);
            DeselectAll();
        }
    }
    Token SelectTokenOnBoard(bool onBoard)
    {
        List<Token> tokenOnBoardList = new List<Token>();

        for (int i = 0; i < tokenList.Count; i++)
        {
            if (tokenList[i].onBoard == onBoard)
            {
                tokenOnBoardList.Add(tokenList[i]);
            }            
        }
        token = tokenOnBoardList[Random.Range(0, tokenOnBoardList.Count-1)];       
        return token;
    }
    BoardCell SelectBoardCell()
    {
        selectedBoardCell = boardCellList[Random.Range(0, boardCellList.Count)];
        return selectedBoardCell;
    }
    void MoveTokenOnBoard(Token token, BoardCell boardCell)
    {
        Debug.Log("<color=blue>NPC try move token:"+token+" on board:"+ boardCell + ".</color>");
        gameBoard.MoveTokenOnBoard(token, boardCell);
        tokenHand.MoveTokenOnBoard(token);
        DeselectAll();
        gameRuler.NextMove();
        Debug.Log("<color=blue>NPC END move</color>");
    }
    void MoveToken(Token token, BoardCell boardCell)
    {
        Debug.Log("<color=blue>NPC try move token:" + token + " :" + boardCell + ".</color>");
        gameBoard.MoveToken(token, boardCell);        
        DeselectAll();
        gameRuler.NextMove();
        Debug.Log("<color=blue>NPC END move</color>");
    }
    void MoveTokenOnToken(Token token, Token token2)
    {
        Debug.Log("<color=blue>NPC try move token:" + token + " on token :" + token2 + ".</color>");
        gameBoard.MoveTokenOnToken(token, token2);       
        DeselectAll();
        gameRuler.NextMove();
        Debug.Log("<color=blue>NPC END move</color>");
    }   
    void MoveTokenTower(Token token, BoardCell boardCell)
    {
        Debug.Log("<color=blue>NPC try move tokenTower:" + token + " : " + boardCell + ".</color>");
        gameBoard.MoveTokenTower(token, boardCell);
        DeselectAll();
        gameRuler.NextMove();
        Debug.Log("<color=blue>NPC END move</color>");
    }
    void MoveTokenTowerOnToken(Token token, Token token2)
    {
        Debug.Log("<color=blue>NPC try move tokenTower:" + token + " on token :" + token2 + ".</color>");
        gameBoard.MoveTokenTowerOnToken(token, token2);
        DeselectAll();
        gameRuler.NextMove();
        Debug.Log("<color=blue>NPC END move</color>");
    }
    void DeselectAll()
    {
        selectedToken = null;
        selectedToken2 = null;
        selectedBoardCell = null;
        selectedTokenTower = null;
    }

    bool CheckTokenOnBoard()
    {
        foreach(var boardCell in gameBoard._boardCellList)
        {
            if(boardCell.tokenList.Count > 0)
            {
                foreach(var token in boardCell.tokenList)
                {
                    if(token.side == Side.npc)
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    bool CheckMoreThanTwoTokenOnBoard()
    {
        List<Token> tokenList = new List<Token>();

        foreach (var boardCell in gameBoard._boardCellList)
        {
            if (boardCell.tokenList.Count > 0)
            {
                foreach (var token in boardCell.tokenList)
                {
                    if (token.side == Side.npc)
                    {
                        tokenList.Add(token);
                    }
                }
            }
        }
        if(tokenList.Count >= 2)
        {
            return true;
        }
        else
        {
            return false;
        }
        
    }
}
