﻿using UnityEngine;
using System.Collections;

public class SetNameClass : MonoBehaviour, ISetName
{
    public void SetName()
    {
        name = "Token:" + Random.Range(1000, 9000);
    }
}