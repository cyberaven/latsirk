﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SetColorClass : MonoBehaviour, ISetColor
{
    public List<Material> _tokenMaterialList;

    public void SetColor(int color)
    {
        if (!_tokenMaterialList[color])
        {
            Debug.Log("No this token color:" + color + " in materialList", gameObject);
        }
        else
        {
            GetComponent<Renderer>().material = _tokenMaterialList[color];
        }
    }
}
