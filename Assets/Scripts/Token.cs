﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Token : TokenBaseClass
{
    public delegate void SelectTokenDelegate(Token token);
    public static event SelectTokenDelegate SelectTokenEvent;

    public delegate void SelectTokenTowerDelegate(Token token);
    public static event SelectTokenTowerDelegate SelectTokenTowerEvent;

    //public bool onBoard = false;
    //public bool selected = false;

    //public Side side;
    float clkTime = 0.8f;
    float timeBtnDown;
    float timeBtnUp;

    //public List<Material> _tokenMaterialList;    
    
    //public void SetSide(Side setSide)
    //{
    //    side = setSide;
    //    SetColorSide();
    //}

    //void SetColorSide()
    //{
    //    if(Side == Side.player)
    //    {
    //        GetComponentInChildren<SpriteRenderer>().color = Color.white;
    //    }
    //    else
    //    {
    //        GetComponentInChildren<SpriteRenderer>().color = Color.black;
    //    }
    //}
    //public void SetColor(int color)
    //{
    //    if (!_tokenMaterialList[color])
    //    {
    //        Debug.Log("No this token color:" + color + " in materialList", gameObject);
    //    }
    //    else
    //    {
    //        GetComponent<Renderer>().material = _tokenMaterialList[color];
    //    }
    //}
    //public void SetPosition(float x, float y, float z)
    //{
    //    transform.localPosition = new Vector3(x,y,z);
    //}

    private void OnMouseDown()
    {
        timeBtnDown = Time.time;       
    }

    private void OnMouseUp()
    {
        timeBtnUp = Time.time;

        if (side != Side.npc)
        {
            if (timeBtnUp - timeBtnDown > clkTime)
            {
                if(SelectTokenTowerEvent != null)
                {
                    SelectTokenTowerEvent(this);
                    Debug.Log("SelectTower");
                    Debug.Log(timeBtnUp + " - "+ timeBtnDown);
                }
            }
            else
            {
                if (SelectTokenEvent != null)
                {
                    SelectTokenEvent(this);
                    Debug.Log("SelectToken");
                    Debug.Log(timeBtnUp + " - " + timeBtnDown);
                }
            }

            timeBtnUp = 0f;
            timeBtnDown = 0f;
        }
    }



    public void StartEvent()
    {
        if (SelectTokenEvent != null)
        {
            SelectTokenEvent(this);
        }
    }
}
