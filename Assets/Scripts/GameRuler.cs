﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameRuler : MonoBehaviour
{    
    public GameBoard _gameBoard;
    [SerializeField] GameBoard gameBoard;
    [SerializeField] BoardCell selectedboardCell;
    [SerializeField] int sizeX = 10;
    [SerializeField] int sizeZ = 10;

    public TokenHand _tokenHand;    
    [SerializeField] TokenHand tokenHand;
    [SerializeField] TokenHand npcTokenHand;
    [SerializeField] Token selectedToken;
    [SerializeField] Token selectedToken2;
    [SerializeField] Token selectedTokenTower;

    public Npc _npc;
    public bool move = true;
   
    private void Start()
    {
        Token.SelectTokenEvent += SelectToken;
        Token.SelectTokenTowerEvent += SelectTokenTower;
        BoardCell.SelectBoardCellEvent += SelectBoardCell;

        StartNewGame();
    }

    private void Update()
    {
        TokenMovement();        
    }

    void StartNewGame()
    {
        gameBoard = Instantiate(_gameBoard);
        gameBoard.CreateBoard(sizeX, sizeZ);

        tokenHand = Instantiate(_tokenHand);
        tokenHand.CreateStartToken(Side.player);       

        npcTokenHand = Instantiate(_tokenHand);
        npcTokenHand.CreateStartToken(Side.npc);        

        Npc npc = Instantiate(_npc);
        npc.InitNpc(this, npcTokenHand, gameBoard);        
    }

    #region SelectToken/SelectToken2/SelectBoardCell
    void SelectToken(Token token)
    {
        if (selectedToken == null)
        {
            selectedToken = token;
            selectedToken.selected = true;
            Debug.Log("Token selected:" + token.name);
        }
        else
        {
            selectedToken2 = token;
            selectedToken2.selected = true;
            Debug.Log("Token2 selected:" + token.name);
        }
    }

    void SelectTokenTower(Token token)
    {
        selectedTokenTower = token;
        selectedToken = null;
        selectedToken2 = null;
        Debug.Log("Token tower selected:" + token.name);
    }
        
    void SelectBoardCell(BoardCell boardCell)
    {
        selectedboardCell = boardCell;
        Debug.Log("BoardCell selected:" + boardCell.name);
    }
    #endregion

    #region TokenMovement
    void TokenMovement()
    {
        if (move == true)
        {
            SelectSimilarToken();
            SelectBoardCellFirst();
            PutTokenInHand();
            MoveTokenOnBoard();
            MoveToken();
            MoveTokenOnToken();
            MoveTokenTower();
            MoveTokenTowerOnToken();
        }
        else
        {
            Debug.Log("<color=red>Not your move.</color>");
        }
    }
    void SelectSimilarToken()
    {
        if(selectedToken != null && selectedToken2 != null && selectedToken == selectedToken2)
        {
            Debug.Log("<color=red>Token similar.</color>");
            DeselectAll();            
        }
    }
    public void SelectBoardCellFirst()
    {
        if(selectedboardCell != null && selectedToken == null && selectedTokenTower == null)
        {
            Debug.Log("<color=red>Not select boardCell first.</color>");
            DeselectAll();            
        }
    }
    void PutTokenInHand()
    {
        if (selectedToken != null && selectedToken2 != null && selectedToken2.onBoard == false)
        {
            Debug.Log("<color=red>Put token in hand not accepted.</color>");
            DeselectAll();           
        }
    }
    void MoveTokenOnBoard()
    {
        if(selectedToken != null && selectedboardCell != null && selectedToken.onBoard == false)
        {           
            gameBoard.MoveTokenOnBoard(selectedToken, selectedboardCell);            
            tokenHand.MoveTokenOnBoard(selectedToken);           
            DeselectAll();           
            NextMove();           
        }
    }
    void MoveToken()
    {
        if (selectedToken != null && selectedboardCell != null && selectedToken.onBoard == true)
        {
            gameBoard.MoveToken(selectedToken, selectedboardCell);                       
            DeselectAll();
            NextMove();
        }
    }
    void MoveTokenOnToken()
    {
        if (selectedToken != null && selectedToken2 != null)
        {
            gameBoard.MoveTokenOnToken(selectedToken, selectedToken2);
            tokenHand.MoveTokenOnBoard(selectedToken);
            DeselectAll();
            NextMove();
        }
    }
    void MoveTokenTower()
    {
        if(selectedTokenTower != null && selectedboardCell != null && selectedTokenTower.onBoard == true)
        {
            gameBoard.MoveTokenTower(selectedTokenTower, selectedboardCell);
            DeselectAll();
            NextMove();
        }
    }
    void MoveTokenTowerOnToken()
    {
        if (selectedTokenTower != null && selectedToken != null && selectedTokenTower.onBoard == true)
        {
            Debug.Log("99999");
            gameBoard.MoveTokenTowerOnToken(selectedTokenTower, selectedToken);
            DeselectAll();
            NextMove();
            Debug.Log("99999");
        }
    }
    #endregion

    public void NextMove()
    {
        if(move == true)
        {
            move = false;
        }
        else if(move == false)
        {
            move = true;
        }
        Debug.Log("<color=green>NextMove</color>");
        
    }
    void DeselectAll()
    {
        if (selectedToken != null)
        {
            selectedToken.selected = false;
            selectedToken = null;
        }
        if (selectedToken2 != null)
        {
            selectedToken2.selected = false;
            selectedToken2 = null;
        }
        selectedboardCell = null;
        selectedTokenTower = null;

        Debug.Log("<color=red>Deselect All</color>");
    }
}
