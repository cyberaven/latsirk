﻿using UnityEngine;
using System.Collections;

public class SetPositionClass : MonoBehaviour, ISetPosition
{
    public void SetPosition(float x, float y, float z)
    {
        transform.localPosition = new Vector3(x, y, z);
    }
}
