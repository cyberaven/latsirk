﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardCell : MonoBehaviour
{
    public Material[] materialArr = new Material[4];

    public delegate void SelectBoardCellDelegate(BoardCell boardCell);
    public static event SelectBoardCellDelegate SelectBoardCellEvent;

    public List<Token> tokenList;

    private void Start()
    {
        SetName();
    }

    void SetName()
    {
        name = "BC X:" + transform.position.x + " Z:" + transform.position.z;
    }
    public void SetPosition(int x, int z)
    {
        transform.localPosition = new Vector3(x,0,z);
    }
    public void SetColor(string color)
    {
        if(color == "random")
        {
            GetComponent<Renderer>().material = materialArr[Random.Range(0, materialArr.Length)];
        }
        else
        {
            Debug.Log("No Color: "+ color, gameObject);
        }
    }
    private void OnMouseDown()
    {
        if(SelectBoardCellEvent != null)
        {
            SelectBoardCellEvent(this);
        }
    }
    public void StartEvent()
    {
        if (SelectBoardCellEvent != null)
        {
            SelectBoardCellEvent(this);
        }
    }

    public void AddTokenInList(Token token)
    {
        Vector3 tlp = transform.localPosition;

        token.transform.SetParent(transform.parent);

        if (tokenList.Count == 0)
        {            
            token.SetPosition(tlp.x, 0.7f, tlp.z);
            Debug.Log("Token:" + token.name + " put onBoard in cell:" + name);
        }
        else
        {
            Token lastTokenInList = tokenList[tokenList.Count - 1];
            Vector3 lastTokenTLP = lastTokenInList.transform.localPosition;
            token.SetPosition(lastTokenTLP.x, lastTokenTLP.y + 0.31f, lastTokenTLP.z);
            Debug.Log("Token:" + token.name + " put onBoard in cell:" + name);
        }

        tokenList.Add(token);
        
    }
    public void RemoveTokenFromList(Token token)
    {
        if (tokenList.Contains(token))
        {
            tokenList.Remove(token);
        }
    }   
}
