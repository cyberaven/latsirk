﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TokenHand : MonoBehaviour
{
    [SerializeField]Side side;
    
    public float _tokenOnStart = 5;
    public int _tokenColorCount = 4;
    public float _tokenPositionOffsetInHand = 0.35f;
    public Token _token;
    public List<Token> _tokenList;

    public GameBoard gameBoard;   

    public void CreateStartToken(Side side)
    {
        for (int x = 0; x < _tokenColorCount; x++)
        {
            for (float y = 0; y < _tokenPositionOffsetInHand * _tokenOnStart; y += _tokenPositionOffsetInHand)
            {
                Token token = Instantiate(_token, transform);
                _tokenList.Add(token);

                token.SetColor(x);
                token.SetPosition(x, y, 0);
                token.SetSide(side);
            }
        }
        SetSide(side);
    }

    public void MoveTokenOnBoard(Token token)
    {
        token.onBoard = true;        
    }      
    public void SetSide(Side setSide)
    {
        side = setSide;
        name = "TokenHand:" + side.ToString();
        SetPosition();
    }
    public void SetPosition()
    {
        if(side == Side.npc)
        {
            transform.position = new Vector3(10.5f,1,6);
        }
        else if(side == Side.player)
        {
            transform.position = new Vector3(-1.5f, 1, 6);
        }
        else
        {
            Debug.Log("No position for type:" + side.ToString(), gameObject);
        }
           
    }

}
