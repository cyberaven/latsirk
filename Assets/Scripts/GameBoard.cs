﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameBoard : MonoBehaviour
{
    public BoardCell _boardCell;
    public List<BoardCell> _boardCellList;

    bool rotateLeft = false;
    bool rotateRight = false;
    float rotateAngle;
    float rotateSpeed = 2f;

    private void Start()
    {
        TurnBoardLeft.StartTurnBoardLeftEvent += StartRotateBoardLeft;
        TurnBoardLeft.StopTurnBoardLeftEvent += StopRotateBoardLeft;
        TurnBoardRight.StartTurnBoardRightEvent += StartRotateBoardRight;
        TurnBoardRight.StopTurnBoardRightEvent += StopRotateBoardRight;
    }
    private void Update()
    {
        RotateLeft();
        RotateRight();
    }

    public void CreateBoard(int sizeX, int sizeZ)
    {
        for (int x = 0; x < sizeX; x++)
        {
            for (int z = 0; z < sizeZ; z++)
            {
                BoardCell boardCell = Instantiate(_boardCell, transform);
                _boardCellList.Add(boardCell);
                boardCell.SetPosition(x, z);
                boardCell.SetColor("random");
            }
        }
    }

    public void MoveTokenOnBoard(Token token, BoardCell boardCell)
    {
        boardCell.AddTokenInList(token);
    }
    public void MoveToken(Token token, BoardCell boardCell)
    {        
        BoardCell bc = SearchBoardCell(token);
        bc.RemoveTokenFromList(token);
        boardCell.AddTokenInList(token);
    }
    public void MoveTokenOnToken(Token token, Token token2)
    {
        if (token.onBoard == false)
        {        
            BoardCell bc2 = SearchBoardCell(token2);           
            bc2.AddTokenInList(token);
        }
        else
        {
            BoardCell bc = SearchBoardCell(token);
            BoardCell bc2 = SearchBoardCell(token2);

            bc.RemoveTokenFromList(token);
            bc2.AddTokenInList(token);
        }
    }

    public void MoveTokenTower(Token token, BoardCell boardCell)
    {        
        BoardCell bc = SearchBoardCell(token);
        List<Token> tokenList = new List<Token>(bc.tokenList);

        for (int i = 0; i < tokenList.Count; i++)
        {           
            bc.RemoveTokenFromList(tokenList[i]);
            boardCell.AddTokenInList(tokenList[i]);
        }        
    }
    public void MoveTokenTowerOnToken(Token token, Token token1)
    {        
        BoardCell bc = SearchBoardCell(token1);
        MoveTokenTower(token, bc);       
    }

    BoardCell SearchBoardCell(Token token)
    {
        BoardCell result = null;            

        foreach (var boardCell in _boardCellList)
        {
            if(boardCell.tokenList.Count > 0)
            {
                foreach(var tokenInList in boardCell.tokenList)
                {
                    if(token == tokenInList)
                    {
                        return result = boardCell;
                    }
                }
            }            
        }
        return result;
    }


    #region RotateBoard
    void StartRotateBoardLeft()
    {
        rotateLeft = true;
        rotateRight = false;
    }
    void StopRotateBoardLeft()
    {
        rotateLeft = false;
    }
    void RotateLeft()
    {
        if (rotateLeft == true)
        {
            transform.RotateAround(new Vector3(4.5f,0f,4.5f), Vector3.up, 1);           
        }
    }
    void StartRotateBoardRight()
    {
        rotateRight = true;
        rotateLeft = false;
    }
    void StopRotateBoardRight()
    {
        rotateRight = false;
    }
    void RotateRight()
    {
        if (rotateRight == true)
        {
            transform.RotateAround(new Vector3(4.5f, 0f, 4.5f), Vector3.up, -1);           
        }
    }
    #endregion

}
