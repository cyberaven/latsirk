﻿using UnityEngine;
using System.Collections;

public class SetColorSideClass : MonoBehaviour, ISetColorSide
{
    public void SetColorSide(Side side)
    {
        if (side == Side.player)
        {
            GetComponentInChildren<SpriteRenderer>().color = Color.white;
        }
        else
        {
            GetComponentInChildren<SpriteRenderer>().color = Color.black;
        }
    }    
}
