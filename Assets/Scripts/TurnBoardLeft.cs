﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TurnBoardLeft : MonoBehaviour
{
    public delegate void StartTurnBoardLeftHandler();
    public static event StartTurnBoardLeftHandler StartTurnBoardLeftEvent;

    public delegate void StopTurnBoardLeftHandler();
    public static event StopTurnBoardLeftHandler StopTurnBoardLeftEvent;

    
    private void OnMouseDown()
    {       
        if(StartTurnBoardLeftEvent != null)
        {
            StartTurnBoardLeftEvent();
        }
    }
    private void OnMouseUp()
    {        
        if (StopTurnBoardLeftEvent != null)
        {
            StopTurnBoardLeftEvent();
        }
    }    
}
